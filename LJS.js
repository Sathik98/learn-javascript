
//datatypes - primitive
/*
//number

var age = 23;
var cgpa = 6.5;

console.log("age: " + age);
console.log("cgpa: "+ cgpa);

//string
var name = "sathik";
var profession = "frontend dev"

console.log(name);
console.log(profession);

//boolean
var yes = true;
var no = false;

console.log(yes);
console.log(no);

//null
var emptyvalue = null;

console.log(typeof emptyvalue);

//undefined
var address;

console.log(address);

var age = 23;
console.log(typeof age);

age = 26;
console.log(typeof age);

age = "i am twenty seven now";

console.log(typeof age);
*/


//operators
/*
//arithmatic
var result = 10 + 20;
console.log(result);

result = result - 5;
console.log(result);

result = result * 2;
console.log(result);

result = result / 5;
console.log(result);

result = result % 3;  //modulus(remainder)
console.log(result);

result = result + 1;
console.log(result);

result++;
console.log(result);

result--;
console.log(result);

var lang = "javascript";

console.log("hello " + lang + "!!!");

console.log(lang + 100);

//relational operator

console.log("10 > 5:", 10 > 5);
console.log("10 < 5:", 10 < 5);

console.log("16 >= 15:", 16 >= 15);
console.log("16 <= 15:", 16 <= 15);

console.log("15 == 15:", 15 == 15);
console.log("5 != 5:", 5 != 5);

console.log("10 == '10':", 10 == '10');
console.log("10 != '10':", 10 != '10')
*/


//function
/*
function functionName(param1, param2){
    //statements

}

function greet1() {
    console.log("welcome");
}

greet1();
greet1();
greet1();
greet1();

function greet2(name, msg) {
    return "hello!!! " + name + msg;
}

var greetmessage = greet2("sathik ali ", "how are you?");
console.log(greetmessage); 

function greet3(message, name) {
    if(name){
        return "hello, " + name + "!!" + message;
    } else {
        return "hello, anonymous! " + message;
    }
}

var greetmessgae = greet3("how are you?", "sathik");

console.log(greetmessgae);


function greet4(message, name){
    if(name){
        return "hello " + name + "!!!" + message;
    }
    console.log("hi i am learning function");
    return "hello, anonymous " + message;
}

var greetmessage = greet4("how are you?");
console.log(greetmessage);
*/


//if and if-else statements
//if statements
/*
var personOneAge = 23;
var personTwoAge = 29;

if (personOneAge > personTwoAge){
    console.log("person one is older than person two");
}
if (personTwoAge > personOneAge){
    console.log("person two is older than person one");
}


//if-else

var personOneAge = 23;
var personTwoAge = 29;

if (personOneAge > personTwoAge){
    console.log("person one is older than person two");
} else {
    console.log("person two is older than person two");
}

var personThreeAge = 40;

if (personOneAge > personTwoAge && personOneAge > personThreeAge){
    console.log("person one is older than person two and three");
}

if (personTwoAge > personOneAge && personTwoAge > personThreeAge){
    console.log("person two is older than person one and  three");
}

if (personThreeAge > personOneAge && personThreeAge > personTwoAge){
    console.log("person three is older than person one and two")
}



var personOneAge = 23;
var personTwoAge = 29;
var personThreeAge = 40;

if(personOneAge > personTwoAge && personOneAge > personThreeAge) {
    console.log("person one is older than person two and three");
} else if (personTwoAge > personThreeAge) {
    console.log("person two is older than person one and three");
} else {
    console.log("person three is older than person one and two");
}

//example

var age = 22;
var parentspermission = true;

if(age >=18 || parentspermission) {
    console.log("your allowed to ride");
} else {
    console.log("your not allowed to ride");
}
*/

/*
var day = 5;

if (day === 1) {
    console.log("yeah! this is monday motivation");
} else if (day === 2) {
    console.log("tuesday is going good");
} else if (day === 3) {
    console.log("wednesday is midday of week");
} else if (day === 4) {
    console.log("day four is also good day ");
} else if (day === 5) {
    console.log("masha allah its friday");
} else if (day === 6) {
    console.log("first holiday");
} else if (day === 7) {
    console.log("yeah its weedend holiday");
}
*/

//switch statements
//example
/*
var days = 1;

switch (days) {
    case 1:
        console.log("yeah! this is monday motivation");
        break;
    case 2:
        console.log("tuesday is going good");
        break;
    case 3:
        console.log("wednesday is midday of week");
        break;
    case 4:
        console.log("day four is also good day");
        break;
    case 5:
        console.log("masha allah its friday");
        break;
    case 6:
        console.log("first holiday");
        break;
    case 7:
        console.log("yeah its weekend holiday");
        break;
    default:
        console.log("week is over and next week is started");
    }
    */


//alert, prompt, confirm
    /*
    var name = prompt("what is your name?", "Anonumous");
    alert("hello " + name + "!!!");
    
    //confirm
    var response = confirm("i am okay with learning JS completely");
    alert(response);
    
    //example
    
    var age = prompt("what is your age?", 12);
    if (age < 18) {
        var parentspermission = confirm("i have parents permission");
        if(parentspermission) {
            alert("your allowed to ride");
        }else {
            alert("your not allowed to ride");   
        }
    } else {
        alert("your allowed to ride");
    }
    */
    

    //do while loop
    /*
    do {
        console.log("playing game");
        var continueplaying = confirm("continue playing?");
    } while (continueplaying);

    console.log("the end");
    */

    //while loop
    /*
    var count = 10;
    while (count > 3) {
        console.log("count: ", count--);
    }

    console.log("while loop ended");
    */

    //for loop
    //syntax
    /*
    for (initializeCounter; Condition; incrementCounter) {
        //block of statements
    }
    
    for (var daysInMonth = 1; daysInMonth <=30; daysInMonth++) {
        console.log("days: ", daysInMonth);
    }
    */

    // break and continue
    


    //object literal
    /*
    const person = {
    firsName: "sathik",
    lastName: "ali",
    age: 23,
    job: "frontend dev",
    };
    person.place= "madurai";

    console.log(person["firsName"] + " " + person['lastName'] + " is " + person['age'] + " years old and his profession is " + person['job'] + " from " + person['place']);
    */

    /*
    const person = {
        firstName: "sathik",
        age: 23,
        job: "developer",
    };

    let txt = " ";
    for (let x in person) {
        txt += person[x] + " ";
     
    }

    console.log(txt);
    */
/*
    let person = {
        firstName: "sathik",
        lastName:"ali",
        age: 23,
        job: "frontend developer",
        siblings: {
            sister1: "sajika",
            sister2: "sabika."
        }
    };

    console.log(" my name is " + person.firstName + " " + person.lastName + " and i am  " + person.age + " years old and my profession is " + person.job + " i have two sisters " + person.siblings.sister1 + " and " + person.siblings.sister2);
    */

    //THIS
    /*
    const person = {
        firstName: "sathik",
        lastName: "ali",
        age: 23,
        job: "developer",
        fullName: function() {
            return this.firstName + " " + this.lastName;
        }
    };

    console.log(person.fullName().toUpperCase());
    */

    //constructor function for laptop object
    /*
    function Person(first, last, age, eye) {
        this.firstName = first;
        this.lastName = last;
        this.age = age;
        this.eyeColor = eye;
      }
      
      const myFather = new Person("John", "Doe", 50, "blue");

      console.log(myFather.age)
      */


      //JavaScript ARRAYS
      /*
      const house = ["living room", "kitchen", "bed room"];
      house[3] = "bathroom";  //accessing array elements.
      house[0] = "Hall";  //changing an array elements.

      console.log(house.length);
      console.log(typeof house);
      console.log(Array.isArray(house));
      console.log(house instanceof Array);
      */
/*
      const man = ["head", "body", "hand", "leg"];
      
      console.log(man);
      console.log(man.toString());
      console.log(man.join(" - "));
      console.log(man.pop());
      console.log(man);
      */

/*
      function myFunction() {
          man.push("finger");               //pushing push()
          console.log(man);
          console.log(man.push("finger"));
      }
*/
      /*
       console.log(man.shift());
       console.log(man); 

       console.log(man.unshift("hair"));
       console.log(man);


       const fruits= ["apple", "banana"];

       console.log("the fruits is: " + fruits[0] + " " + fruits[1]);
       delete fruits[0];
       console.log("the fruits is: " + fruits[0] + " " + fruits[1]);
       */
/*
       const fruits = ["apple", "banana", "orange",];

       console.log(fruits);
       console.log(fruits.splice(2, 1, "grapes", "mango"));
       console.log(fruits);

*/


